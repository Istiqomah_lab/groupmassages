//
//  ViewController.swift
//  GroupMessages
//
//  Created by User on 2019/7/9.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

struct ChatMessage {
    let text: String
    let isComing:Bool
    let date: Date
}
class ViewController: UIViewController {

    //MARK: - Properties
    var tableView: UITableView!
    
    fileprivate let cellid = "cellID"
    
    /*
    let chatMessages = [
        [ChatMessage(text: "Here's the first message", isComing: true, date: Date.dateFromCustomString(custonString: "07/07/2019")),
     ChatMessage(text: "i'm going to message another ling message that will word wrap", isComing: true, date: Date.dateFromCustomString(custonString: "07/07/2019"))],
    [ChatMessage(text: "i'm going to message another ling message that will word wrap", isComing: false, date: Date.dateFromCustomString(custonString: "08/07/2019")),ChatMessage(text: "i'm going to message another ling message that will word wrap. i'm going to message another ling message that will word wrap. i'm going to message another ling message that will word wrap. i'm going to message another ling message that will word wrap", isComing: false, date: Date()),ChatMessage(text: "hai whatupp??", isComing: true, date: Date.dateFromCustomString(custonString: "08/07/2019"))],
    [ChatMessage(text: "I'm fine, how about you", isComing: false, date: Date.dateFromCustomString(custonString: "09/07/2019")),
     ChatMessage(text: "Great, what are you going to do today?", isComing: true, date: Date.dateFromCustomString(custonString: "09/07/2019"))]
    ]
     */
    
    let messagesFromServer = [
         ChatMessage(text: "Here's the first message", isComing: true, date: Date.dateFromCustomString(custonString: "07/07/2019")),
         ChatMessage(text: "i'm going to message another ling message that will word wrap", isComing: true, date: Date.dateFromCustomString(custonString: "07/07/2019")),
         ChatMessage(text: "i'm going to message another ling message that will word wrap", isComing: false, date: Date.dateFromCustomString(custonString: "07/08/2019")),ChatMessage(text: "i'm going to message another ling message that will word wrap. i'm going to message another ling message that will word wrap. i'm going to message another ling message that will word wrap. i'm going to message another ling message that will word wrap", isComing: false, date: Date.dateFromCustomString(custonString: "07/08/2019")),ChatMessage(text: "hai whatupp??", isComing: true, date: Date.dateFromCustomString(custonString: "07/08/2019")),ChatMessage(text: "I'm fine, how about you", isComing: false, date: Date.dateFromCustomString(custonString: "07/09/2019")),
         ChatMessage(text: "Great, what are you going to do today?", isComing: true, date: Date.dateFromCustomString(custonString: "07/09/2019"))
    ]
    fileprivate func attempToAssambleGroupedMessages(){
        let groupMessages = Dictionary(grouping: messagesFromServer) { (element) -> Date in
            return element.date
        }
        let sortedKeys = groupMessages.keys.sorted()
        sortedKeys.forEach { (key) in
            let value = groupMessages [key]
            chatMessages.append(value ?? [])
        }
        
    }
    
    var chatMessages = [[ChatMessage]]()
    
    
    //MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        attempToAssambleGroupedMessages()
        view.backgroundColor = .white
        // Do any additional setup after loading the view, typically from a nib.
        
        navigationItem.title = "Messages"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ChatMessanger.self, forCellReuseIdentifier: cellid)
        tableView.backgroundColor = .groupTableViewBackground
        tableView.separatorStyle = .none
        view.addSubview(tableView)
        tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return chatMessages.count
    }
    
    class DateHeaderLabel: UILabel {
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            backgroundColor = .black
            textColor = .white
            textAlignment = .center
            translatesAutoresizingMaskIntoConstraints = false
            font = UIFont.systemFont(ofSize: 14)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        override var intrinsicContentSize: CGSize{
            let originalContentSize = super.intrinsicContentSize
            let height = originalContentSize.height + 12
            layer.cornerRadius = height / 2
            layer.masksToBounds = true
            return CGSize(width: originalContentSize.width + 20, height: height)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = DateHeaderLabel()
        
        if let firstMessageInSection = chatMessages[section].first {
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "MM/dd/YYYY"
            let dataString = dateFormater.string(from: firstMessageInSection.date)
            label.text = dataString
        }
        
        let containerHeader = UIView()
        containerHeader.addSubview(label)
        label.centerInSuperview()
        
        return containerHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    /*
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let firstMessageInSection = chatMessages[section].first {
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "MM/dd/YYYY"
            let dataString = dateFormater.string(from: firstMessageInSection.date)
            return  dataString
        }
        return "section: \(Date())"
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessages[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! ChatMessanger
        let chatMessage = chatMessages[indexPath.section][indexPath.row]
        cell.chatMassage = chatMessage
        
        return cell
    }
    
    
}
extension Date {
    static func dateFromCustomString(custonString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.date(from: custonString) ?? Date()
    }
}
