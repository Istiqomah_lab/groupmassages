//
//  ChatMessanger.swift
//  GroupMessages
//
//  Created by User on 2019/7/9.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class ChatMessanger: UITableViewCell {

    var leadingConstraint : NSLayoutConstraint!
    var trailingConstraint : NSLayoutConstraint!
    var messageLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.text = "we want to provide a longer string that is actually going to wrap onto the nexy line and maybe even a third line"
        return label
    }()
    
    var bubbleBackground : UIView = {
       let bv = UIView()
        bv.backgroundColor = UIColor.white
        bv.layer.cornerRadius = 12
        return bv
    }()
    var chatMassage: ChatMessage!{
        didSet{
            bubbleBackground.backgroundColor = chatMassage.isComing ? .white: .darkGray
            messageLabel.textColor = chatMassage.isComing ? .darkGray : .white
            
            messageLabel.text = chatMassage.text
            
            leadingConstraint.isActive = chatMassage.isComing
            trailingConstraint.isActive = !chatMassage.isComing
                
           
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        
        addSubview(bubbleBackground)
        addSubview(messageLabel)
        messageLabel.anchor(top: topAnchor, leading: nil, trailing: nil, bottom: bottomAnchor, padding: UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20))
        messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250).isActive = true
        
        leadingConstraint = messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20)
        trailingConstraint = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        
        bubbleBackground.anchor(top: messageLabel.topAnchor, leading: messageLabel.leadingAnchor, trailing: messageLabel.trailingAnchor, bottom: messageLabel.bottomAnchor, padding: UIEdgeInsets(top: -10, left: -10, bottom: -10, right:-10) )
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
